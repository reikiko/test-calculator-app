import Label from "../Label/Label";

const Layout = ({ children, containerClassName, labelClassName }) => {
  return (
    <div
      className={`box-border min-h-screen w-[20em] mx-auto bg-light-theme dark:bg-dark-theme overflow-hidden font-main ${containerClassName}`}
    >
      <Label
        text={"DEVELOPMENT"}
        className={`bg-btn-light-gray dark: ${labelClassName}`}
      />
      <div className="px-3">{children}</div>
    </div>
  );
};

export default Layout;
