import { render, screen } from "@testing-library/react";
import App from "./App";

test("tes", () => {
  render(<App />);
  const linkElement = screen.getByText(/a/i);
  expect(linkElement).toBeInTheDocument();
});

