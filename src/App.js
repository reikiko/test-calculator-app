import "./App.css";
import Button from "./components/Button/Button";
import Layout from "./components/Layout/Layout";
import SwitchToggle from "./components/SwitchToggle/SwitchToggle";
import plusminusblack from "./assets/Icon/plusminus-black.svg";
import plusminuswhite from "./assets/Icon/plusminus-white.svg";
import delblack from "./assets/Icon/del-black.svg";
import delwhite from "./assets/Icon/del-white.svg";
import { useEffect, useState } from "react";

function App() {
  const [userTheme, setUserTheme] = useState(false);
  const [calc, setCalc] = useState("");
  const [result, setResult] = useState("");
  const [display, setDisplay] = useState("");
  const [fontses, setFontses] = useState(72);

  const ops = ["/", "*", "+", "-", "."];

  const updateCalc = (value) => {
    if (
      (ops.includes(value) && calc === "") ||
      (ops.includes(value) && ops.includes(calc.slice(-1)))
    ) {
      return;
    }

    setCalc(calc + value);

    if (!ops.includes(value)) {
      // eslint-disable-next-line
      setResult(eval(calc + value).toLocaleString("id-ID"));
    }
  };

  const calculate = () => {
    // eslint-disable-next-line
    setCalc(eval(calc).toLocaleString("id-ID"));
  };

  const deleteLast = () => {
    if (calc === "") {
      return;
    }
    const value = calc.slice(0, -1);
    setCalc(value);
    setResult(value);
  };

  const plusminus = () => {
    if (calc.charAt(0) === "-" || result.charAt(0) === "-") {
      setCalc(calc.substring(1));
      setResult(result.substring(1));
    } else {
      setCalc("-" + calc);
    }
  };

  const percent = () => {
    setResult(String(parseFloat(result) / 100));
    setCalc(result + "/100");
  };

  const reset = () => {
    setCalc("");
    setResult("0");
  };

  useEffect(() => {
    const root = window.document.documentElement;

    if (!userTheme) {
      root.classList.add("dark");
    } else {
      root.classList.remove("dark");
    }
  }, [userTheme]);

  useEffect(() => {
    if (calc.includes("*")) {
      setDisplay(calc.replaceAll("*", "x"));
      return;
    } else {
      setDisplay(calc);
    }

    if (calc.includes("/")) {
      setDisplay(calc.replaceAll("/", "÷"));
      return;
    } else {
      setDisplay(calc);
    }
  }, [calc]);

  useEffect(() => {
    const newResult = result.replaceAll(".", "");

    if (newResult.length === 7) {
      setFontses(60);
    } else if (newResult.length === 8) {
      setFontses(54);
    } else if (newResult.length === 9) {
      setFontses(48);
    } else if (newResult.length > 9) {
      setFontses(40);
    } else {
      setFontses(72);
    }
  }, [result]);

  return (
    <Layout containerClassName={""}>
      <div className="flex justify-center">
        <SwitchToggle enabled={userTheme} setEnabled={setUserTheme} />
      </div>

      <div className="flex flex-col text-right mt-20 mb-5">
        <div className="text-3xl font-extralight text-neutral-400 mb-4">
          {display || "0"}
        </div>
        <div
          className={`font-light dark:text-white`}
          style={{ fontSize: `${fontses}px` }}
        >
          {result || "0"}
        </div>
      </div>

      <div className="flex flex-wrap justify-around gap-3">
        <Button
          className={
            "bg-btn-light-gray text-black dark:bg-btn-dark-gray-1 dark:text-white"
          }
          text={"C"}
          onClick={reset}
        />
        <Button
          className={
            "bg-btn-light-gray text-black dark:bg-btn-dark-gray-1 dark:text-white"
          }
          icon={userTheme ? plusminusblack : plusminuswhite}
          onClick={plusminus}
        />
        <Button
          className={
            "bg-btn-light-gray text-black dark:bg-btn-dark-gray-1 dark:text-white "
          }
          text={"%"}
          onClick={percent}
        />
        <Button
          className={"bg-btn-blue text-white"}
          text={"÷"}
          onClick={() => updateCalc("/")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"7"}
          onClick={() => updateCalc("7")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"8"}
          onClick={() => updateCalc("8")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"9"}
          onClick={() => updateCalc("9")}
        />
        <Button
          className={"bg-btn-blue text-white"}
          text={"x"}
          onClick={() => updateCalc("*")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"4"}
          onClick={() => updateCalc("4")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"5"}
          onClick={() => updateCalc("5")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"6"}
          onClick={() => updateCalc("6")}
        />
        <Button
          className={"bg-btn-blue text-white"}
          text={"-"}
          onClick={() => updateCalc("-")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"1"}
          onClick={() => updateCalc("1")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"2"}
          onClick={() => updateCalc("2")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"3"}
          onClick={() => updateCalc("3")}
        />
        <Button
          className={"bg-btn-blue text-white"}
          text={"+"}
          onClick={() => updateCalc("+")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={","}
          onClick={() => updateCalc(".")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          text={"0"}
          onClick={() => updateCalc("0")}
        />
        <Button
          className={
            "bg-white text-black dark:bg-btn-dark-gray-2 dark:text-white"
          }
          icon={userTheme ? delblack : delwhite}
          onClick={deleteLast}
        />
        <Button
          className={"bg-btn-blue text-white"}
          text={"="}
          onClick={calculate}
        />
      </div>
    </Layout>
  );
}

export default App;
